﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace Simplex_Method
{
    
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private int varQuantity = 7,
            restrictionQuantity = 7;
        private TextBox[] funcCoefVarBox,
            freeVarBox;
        private ComboBox[] signComboBox;
        private RadioButton minRadioButton,
            maxRadioButton;
        private double[,] restrictionVar;
        private double[] mainFuncVar,
            freeVar;
        private Sign[] signs;
        private TextBox[,] restrictionCoefVarBox;
        private Aim aim;


        public MainPage()
        {
            this.InitializeComponent();
            
        }

        private void conditionsButton_Click(object sender, RoutedEventArgs e)
        {
            //Clear app
            mainGrid.Children.Clear();

            generateConditionsForm();
        }

        private void generateConditionsForm()
        {
            //Resizing grid
            grid.Height = 1200 + restrictionQuantity * 100;
            grid.Width = 1900 + varQuantity * 50;

            //Creating "F(x) = " TextBlock 
            TextBlock funcTitleText = new TextBlock();
            funcTitleText.Text = "F(x) = ";
            funcTitleText.VerticalAlignment = VerticalAlignment.Top;
            funcTitleText.HorizontalAlignment = HorizontalAlignment.Left;
            funcTitleText.Height = 32;
            funcTitleText.FontSize = 30;
            funcTitleText.Margin = new Thickness(50, 80, 0, 0);
            mainGrid.Children.Add(funcTitleText);

            //Creating main function var input boxes
            funcCoefVarBox = new TextBox[varQuantity];
            for (int i = 0; i < varQuantity; i++)
            {
                
                funcCoefVarBox[i] = new TextBox();
                funcCoefVarBox[i].VerticalAlignment = VerticalAlignment.Top;
                funcCoefVarBox[i].HorizontalAlignment = HorizontalAlignment.Left;
                funcCoefVarBox[i].Height = 32;
                funcCoefVarBox[i].InputScope = setScope();
                funcCoefVarBox[i].Width = funcCoefVarBox[i].MinWidth;
                funcCoefVarBox[i].Margin = new Thickness(i * 150 + 150, 80, 0, 0);

                mainGrid.Children.Add(funcCoefVarBox[i]);
            }

            //Creating main function "*Xn +" Text Blocks
            TextBlock[] varNameText = new TextBlock[varQuantity];
            for (int i = 0; i < varQuantity; i++)
            {
                varNameText[i] = new TextBlock();
                if (i != varQuantity - 1)
                    varNameText[i].Text = "*X" + (i + 1) + " + ";
                else varNameText[i].Text = "*X" + (i + 1) + " =>";
                varNameText[i].VerticalAlignment = VerticalAlignment.Top;
                varNameText[i].HorizontalAlignment = HorizontalAlignment.Left;
                varNameText[i].Height = 32;
                varNameText[i].FontSize = 30;
                varNameText[i].Margin = new Thickness(i * 150 + 220, 80, 0, 0);

                mainGrid.Children.Add(varNameText[i]);

            }

            //Creating main function MIN & MAX radio buttons

            minRadioButton = new RadioButton();
           
            minRadioButton.VerticalAlignment = VerticalAlignment.Top;
            minRadioButton.HorizontalAlignment = HorizontalAlignment.Left;
            minRadioButton.Content = " MIN";
            minRadioButton.FontSize = 25;
            minRadioButton.Margin = new Thickness(varQuantity * 150 + 200, 80, 0, 0);
            minRadioButton.Click += minRadioButton_Click;
            mainGrid.Children.Add(minRadioButton);

            maxRadioButton = new RadioButton();
            maxRadioButton.VerticalAlignment = VerticalAlignment.Top;
            maxRadioButton.HorizontalAlignment = HorizontalAlignment.Left;
            maxRadioButton.Content = " MAX";
            maxRadioButton.FontSize = 25;
            maxRadioButton.Margin = new Thickness(varQuantity * 150 + 300, 80, 0, 0);
            maxRadioButton.Click += maxRadioButton_Click;

            mainGrid.Children.Add(maxRadioButton);

            //Creating system curly bracket
            curlyBracket.Margin = new Thickness(50, 415, 0, 0);
            curlyBracket.Height = (300 * restrictionQuantity) / 4;
            curlyBracket.Visibility = Visibility.Visible;
            
            //Creating restriction functions var input boxes
            restrictionCoefVarBox = new TextBox[restrictionQuantity, varQuantity];
            for (int j = 0; j < restrictionQuantity; j++)
                for (int i = 0; i < varQuantity; i++)
                {

                    restrictionCoefVarBox[j, i] = new TextBox();
                    restrictionCoefVarBox[j, i].VerticalAlignment = VerticalAlignment.Top;
                    restrictionCoefVarBox[j, i].HorizontalAlignment = HorizontalAlignment.Left;
                    restrictionCoefVarBox[j, i].Height = 32;
                    restrictionCoefVarBox[j, i].Width = funcCoefVarBox[i].MinWidth;
                    restrictionCoefVarBox[j, i].InputScope = setScope();
                    restrictionCoefVarBox[j, i].Margin = new Thickness(i * 150 + 150, j * 80 + 160, 0, 0);

                    mainGrid.Children.Add(restrictionCoefVarBox[j, i]);
                }

            //Creating restriction functions "*Xn +" Text Blocks
            TextBlock[,] restrictionVarNameText = new TextBlock[restrictionQuantity, varQuantity];
            for (int j = 0; j < restrictionQuantity; j++)
                for (int i = 0; i < varQuantity; i++)
                {
                    restrictionVarNameText[j, i] = new TextBlock();
                    if (i != varQuantity - 1)
                        restrictionVarNameText[j, i].Text = "*X" + (i + 1) + " + ";
                    else restrictionVarNameText[j, i].Text = "*X" + (i + 1);
                    restrictionVarNameText[j, i].VerticalAlignment = VerticalAlignment.Top;
                    restrictionVarNameText[j, i].HorizontalAlignment = HorizontalAlignment.Left;
                    restrictionVarNameText[j, i].Height = 32;
                    restrictionVarNameText[j, i].FontSize = 30;
                    restrictionVarNameText[j, i].Margin = new Thickness(i * 150 + 220, j * 80 + 160, 0, 0);

                    mainGrid.Children.Add(restrictionVarNameText[j, i]);

                }

            //Creating restriction functions sign ComboBoxes
            signComboBox = new ComboBox[restrictionQuantity];
            List<string> signs = new List<string>()
            {
                "=",
                ">=",
                "<=",
            };
            for (int j = 0; j < restrictionQuantity; j++)
            {
                signComboBox[j] = new ComboBox();
                signComboBox[j].ItemsSource = signs;
                signComboBox[j].VerticalAlignment = VerticalAlignment.Top;
                signComboBox[j].HorizontalAlignment = HorizontalAlignment.Left;              
                signComboBox[j].Margin = new Thickness(varQuantity * 150 + 150, j * 80 + 160, 0, 0);
                signComboBox[j].Height = 32;
                mainGrid.Children.Add(signComboBox[j]);
            }

            //Creating restriction functions free elements input boxes
            freeVarBox = new TextBox[restrictionQuantity];
            for (int j = 0; j < restrictionQuantity; j++)
            {
                freeVarBox[j] = new TextBox();
                freeVarBox[j].VerticalAlignment = VerticalAlignment.Top;
                freeVarBox[j].HorizontalAlignment = HorizontalAlignment.Left;
                freeVarBox[j].Height = 32;
                freeVarBox[j].InputScope = setScope();
                freeVarBox[j].Margin = new Thickness(varQuantity * 150 + 250, j * 80 + 160, 0, 0);

                mainGrid.Children.Add(freeVarBox[j]);

            }

            //Creating button for solve problem
            Button solveButton = new Button();
            solveButton.Tag = "solveButton";
            solveButton.VerticalAlignment = VerticalAlignment.Top;
            solveButton.HorizontalAlignment = HorizontalAlignment.Left;
            solveButton.Width = 200;
            solveButton.Content = "Solve problem";
            solveButton.Margin = new Thickness(50, 160 + (restrictionQuantity) * 80, 0, 0);
            solveButton.Click += solveButton_Click;
            
            mainGrid.Children.Add(solveButton);
        }
        
        private void minRadioButton_Click(object sender, RoutedEventArgs e)
        {
            //Get main function aim
            aim = Aim.MIN;
        }

        private void maxRadioButton_Click(object sender, RoutedEventArgs e)
        {
            //Get main function aim
            aim = Aim.MAX;
        }

        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            //Clear app
            mainGrid.Children.Clear();
            curlyBracket.Visibility = Visibility.Collapsed;

            TextBox resultBox = new TextBox();
            resultBox.VerticalAlignment = VerticalAlignment.Top;
            resultBox.HorizontalAlignment = HorizontalAlignment.Left;
            resultBox.Height = 600;
            resultBox.Width = 1420;
            resultBox.IsReadOnly = true;
            resultBox.TextWrapping = TextWrapping.Wrap;
            resultBox.FontSize = 30;
            resultBox.Margin = new Thickness(50, 80, 0, 0);


            SimplexMethod simplexMethod = new SimplexMethod(varQuantity, restrictionQuantity);
            simplexMethod.solveProblem();
            resultBox.Text = simplexMethod.resultString;

            mainGrid.Children.Add(resultBox);
        }

        private void solveButton_Click(object sender, RoutedEventArgs e)
        {
            getFormInfo();

            TextBox resultBox = new TextBox();
            resultBox.Name = "resultBox";
            resultBox.VerticalAlignment = VerticalAlignment.Top;
            resultBox.HorizontalAlignment = HorizontalAlignment.Left;
            resultBox.Height = 600;
            resultBox.Width = 1420;
            resultBox.IsReadOnly = true;
            resultBox.TextWrapping = TextWrapping.Wrap;
            resultBox.FontSize = 30;
            resultBox.Margin = new Thickness(50, 160 + (restrictionQuantity + 1) * 80, 0, 0);

            SimplexMethod simplexMethod = new SimplexMethod(varQuantity, restrictionQuantity, restrictionVar, mainFuncVar, freeVar, signs, aim);
            simplexMethod.solveProblem();
            resultBox.Text = simplexMethod.resultString;

            mainGrid.Children.Add(resultBox);

        }

        private void varQuantityBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!varQuantityBox.Text.Equals("") && !varQuantityBox.Text.Equals("-"))
                varQuantity = int.Parse(varQuantityBox.Text);
        }

        private void restrictionQuantityBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!restrictionQuantityBox.Text.Equals(""))
                restrictionQuantity = System.Convert.ToInt32(restrictionQuantityBox.Text);
        }
        
        private void getFormInfo()
        {
            //Get main function var into array
            mainFuncVar = new double[varQuantity];
            for (int i = 0; i < varQuantity; i++)
            {
                if (funcCoefVarBox[i].Text != "")
                    mainFuncVar[i] = System.Convert.ToDouble(funcCoefVarBox[i].Text);
                else mainFuncVar[i] = 0f;
            }

            //Get restriction functions var into array
            restrictionVar = new double[restrictionQuantity, varQuantity];
            for (int j = 0; j < restrictionQuantity; j++)
                for (int i = 0; i < varQuantity; i++)
                {
                    if (restrictionCoefVarBox[j, i].Text != "")
                        restrictionVar[j, i] = System.Convert.ToDouble(restrictionCoefVarBox[j, i].Text);
                    else restrictionVar[j, i] = 0f;
                }

            //Get restriction functions free var into array
            freeVar = new double[restrictionQuantity];
            for (int j = 0; j < restrictionQuantity; j++)
            {
                if (freeVarBox[j].Text != "")
                    freeVar[j] = System.Convert.ToDouble(freeVarBox[j].Text);
                else freeVar[j] = 0f;
            }

            //Get restriction functions signs 
            signs = new Sign[restrictionQuantity];
            for (int j = 0; j < restrictionQuantity; j++)
            {
                if (signComboBox[j].SelectedValue as string == "=")
                    signs[j] = Sign.equally;
                else if (signComboBox[j].SelectedValue as string == "<=")
                    signs[j] = Sign.lessThan;
                else if (signComboBox[j].SelectedValue as string == ">=")
                    signs[j] = Sign.greaterThan;
            }
        }
        
        private InputScope setScope()
        {
            var inputScope = new InputScope();
            var inputScopeName = new InputScopeName();
            inputScopeName.NameValue = InputScopeNameValue.NumberFullWidth;
            inputScope.Names.Add(inputScopeName);

            return inputScope;
        }

    }
    public class SimplexMethod
    {
        private int varQuantity;
        private int restrictionQuantity;
        private int basicVarQuantity = 0;
        private int nonBasicVarQuantity;
        private int additionalVarQuantity = 0;

        private int[] basicIndices;
        private int[] nonBasicIndices;

        private double funcFreeVarRate;
        private double mainElement;

        private double[,] restrictionVar;
        private double[] mainFuncVar;
        private double[] freeVar;
        private double[] funcVarRate;

        private bool solvability = true;
        
        private bool[] basicCollums;
        

        private Aim aim;
        private Sign[] signs;

        public string resultString = "";

        public SimplexMethod(int varQuantity, int restrictionQuantity, double[,] restrictionVar, double[] mainFuncVar, double[] freeVar, Sign[] signs, Aim aim)
        {
            this.restrictionVar = restrictionVar;
            this.mainFuncVar = mainFuncVar;
            this.freeVar = freeVar;
            this.signs = signs;
            this.varQuantity = varQuantity;
            this.restrictionQuantity = restrictionQuantity;
            this.aim = aim;
        }
        public SimplexMethod(int varQuantity, int restrictionQuantity)
        {
            this.varQuantity = varQuantity;
            this.restrictionQuantity = restrictionQuantity;
            problemGenerator();
        }

        public void solveProblem()
        {
            
            printNonCanonicSys();
            convertToCanonic();
            resultString += "\nConvert to canonic:";
            printSys();
            if (searchForBasic())
            {
                //resultString += "\nbasic:\n";
                //foreach (bool tmp in basicCollums)
                //{
                //    if (tmp) resultString += "true\t";
                //    else resultString += "fasle\t";
                //}
                //resultString += "\n";
                //foreach (int tmp in basicIndices)
                //{
                //    resultString += tmp + "\t";
                //}
                //resultString += "\n";
                //foreach (int tmp in nonBasicIndices)
                //{
                //    resultString += tmp + "\t";
                //}
                simplexRate();
                resultString += "Iteration №1";
                printTable();
                int mainElementI,
                    mainElementJ;

                int iterator = 1;
                while (funcVarRate.Any(i => i < 0))//Checking for negative elements
                {
                    if (freeVar.All(i => i < 0))//Checking for system solvability
                    {
                        resultString += "The problem has no solution, because all the free variebles are negative.";
                        solvability = false;
                        break;
                    }

                    //Intermediate result output
                    printResult();


                    iterator++;
                    //Finding main elements
                    mainElementI = Array.IndexOf<double>(funcVarRate, funcVarRate.Min());
                    mainElementJ = minSimplexRatio(mainElementI);

                    //System conversion by  Jordan - Gauss algorithm
                    JordanConversion(mainElementJ, mainElementI);

                    //Swaping main element indices
                    int tmp = basicIndices[mainElementJ];
                    basicIndices[mainElementJ] = nonBasicIndices[mainElementI];
                    nonBasicIndices[mainElementI] = tmp;

                    resultString += "\nIteration №" + iterator;

                    //Simplex table output
                    printTable();
                }
                //Final result output
                if (solvability)
                {
                    resultString += "Final result:";
                    printResult();
                }
                    
            }




        }

        private void printNonCanonicSys()
        {
            string streamString = "";
            streamString += "The initial problem:\n";
            //Printing main function
            streamString += "F(x) = ";
            for (int i = 0; i < varQuantity; i++)
            {
                if (i != varQuantity - 1)
                    streamString += "(" + mainFuncVar[i] + ")" + "*X" + (i + 1) + " + ";
                else streamString += "(" + mainFuncVar[i] + ")" + "*X" + (i + 1) + " => ";
            }
            switch (aim)
            {
                case Aim.MAX:
                    streamString += "MAX\n";
                    break;
                case Aim.MIN:
                    streamString += "MIN\n";
                    break;
            }
            streamString += "\n";
            //Printing restriction functions
            for (int j = 0; j < restrictionQuantity; j++)
            {
                for (int i = 0; i < varQuantity; i++)
                {
                    if (i != varQuantity - 1)
                        streamString += "(" + restrictionVar[j, i] + ")" + "*X" + (i + 1) + " + ";
                    else
                    {
                        streamString += "(" + restrictionVar[j, i] + ")" + "*X" + (i + 1);
                        if (signs[j] == Sign.equally) streamString += " = " + freeVar[j];
                        else if (signs[j] == Sign.greaterThan) streamString += " >= " + freeVar[j];
                        else if (signs[j] == Sign.lessThan) streamString += " <= " + freeVar[j];
                    }
                }
                streamString += "\n";
            }

            //Saving system to result string
            resultString += streamString;
        }

        public void printSys()
        {
            string streamString = "";
            for (int i = 0, h = 0; i < varQuantity; i++)
            {
                if (i == varQuantity - 1 && mainFuncVar[i] != 0)
                {
                    streamString += " + (" + mainFuncVar[i] + ")*X" + (i + 1) + "=>";
                    switch (aim)
                    {
                        case Aim.MAX: streamString += " MAX "; break;
                        case Aim.MIN: streamString += " MIN "; break;
                    }
                }
                else if (mainFuncVar[i] != 0)
                {
                    if (h.Equals(0))
                    {
                        streamString += "(" + mainFuncVar[i] + ") * X" + (i + 1);
                        h++;
                    }
                    else streamString += " + (" + mainFuncVar[i] + ")*X" + (i + 1);
                }
                else if(i == varQuantity - 1)
                {
                    streamString += "=>";
                    switch (aim)
                    {
                        case Aim.MAX: streamString += " MAX "; break;
                        case Aim.MIN: streamString += " MIN "; break;
                    }
                }
            }
            streamString += "\n\n";
            for (int j = 0; j < restrictionQuantity; j++)
            {
                for (int i = 0, h = 0; i < varQuantity + additionalVarQuantity; i++)
                {
                    if (i == varQuantity + additionalVarQuantity - 1 && !restrictionVar[j, i].Equals(0))
                        streamString += " + (" + restrictionVar[j, i] + ")*X" + (i + 1) + " = " + freeVar[j];
                    else if (restrictionVar[j, i] != 0)
                    {
                        if (h.Equals(0))
                        {
                            streamString += "(" + restrictionVar[j, i] + ")*X" + (i + 1);
                            h++;
                        }
                        else streamString += " + (" + restrictionVar[j, i] + ")*X" + (i + 1);

                    }
                    else if (i == varQuantity + additionalVarQuantity - 1) streamString += " = " + freeVar[j];
                }
                streamString += "\n";
            }
            streamString += "\n";
            resultString += "\n" + streamString;
            
            
        }
                        
        public void problemGenerator()
        {
            
            restrictionVar = new double[restrictionQuantity, varQuantity];
            mainFuncVar = new double[varQuantity];
            signs = new Sign[restrictionQuantity];
            freeVar = new double[restrictionQuantity];

            Random rand = new Random(DateTime.Now.Millisecond);
            if(rand.Next(0, 1).Equals(1))
            {
                aim = Aim.MAX;
                for (int j = 0; j < restrictionQuantity; j++)
                    if (rand.Next(0, 1).Equals(1)) signs[j] = Sign.lessThan;
                    else signs[j] = Sign.greaterThan;
                for (int i = 0; i < varQuantity; i++)
                    mainFuncVar[i] = rand.Next(-50, 50);
                for (int j = 0; j < restrictionQuantity; j++)
                    for (int i = 0; i < varQuantity; i++)
                        restrictionVar[j, i] = rand.Next(-50, 50);
                for (int j = 0; j < restrictionQuantity; j++)
                    if (signs[j].Equals(Sign.lessThan))
                        freeVar[j] = rand.Next(0, 50);
                    else freeVar[j] = rand.Next(-50, 0);
            }
            else
            {
                aim = Aim.MIN;
                for (int j = 0; j < restrictionQuantity; j++)
                    if (rand.Next(0, 1).Equals(1)) signs[j] = Sign.lessThan;
                    else signs[j] = Sign.greaterThan;
                for (int i = 0; i < varQuantity; i++)
                    mainFuncVar[i] = rand.Next(-50, 50);
                for (int j = 0; j < restrictionQuantity; j++)
                    for (int i = 0; i < varQuantity; i++)
                        restrictionVar[j, i] = rand.Next(-50, 50);
                for (int j = 0; j < restrictionQuantity; j++)
                    if (signs[j].Equals(Sign.lessThan))
                        freeVar[j] = rand.Next(-50, 0);
                    else freeVar[j] = rand.Next(0, 50);
            }
        }

        private void convertToCanonic()
        {
            
            //if aim is MIN convert to MAX
            if(aim.Equals(Aim.MIN))
            {
                for (int i = 0; i < varQuantity; i++)
                    mainFuncVar[i] *= -1f;
                for (int j = 0; j < restrictionQuantity; j++)
                {
                    if (signs[j].Equals(Sign.greaterThan))
                        signs[j] = Sign.lessThan;
                    else if (signs[j].Equals(Sign.lessThan))
                        signs[j] = Sign.greaterThan;
                    aim = Aim.MAX;
                }

            }
            //if free var is negative convert to positive
            for (int j = 0; j < restrictionQuantity; j++)
                if(freeVar[j] < 0)
                {
                    if (signs[j].Equals(Sign.greaterThan))
                        signs[j] = Sign.lessThan;
                    else if (signs[j].Equals(Sign.lessThan))
                        signs[j] = Sign.greaterThan;
                    for (int i = 0; i < varQuantity; i++)
                        restrictionVar[j, i] *= -1f;
                    freeVar[j] *= -1f;
                }

            //Counting quantity of basic collums
            for (int j = 0; j < restrictionQuantity; j++)
                if (signs[j].Equals(Sign.lessThan) || signs[j].Equals(Sign.greaterThan))
                    additionalVarQuantity++;

            //Adding basic
            int basicVarPos = 0;
            for (int j = 0; j < restrictionQuantity; j++)
            {
                arr2DResize<double>(ref restrictionVar, restrictionQuantity, varQuantity + additionalVarQuantity);
                if (signs[j].Equals(Sign.equally))
                    for (int i = 0; i < additionalVarQuantity; i++)
                        restrictionVar[j, varQuantity + i] = 0f;
                else if (signs[j].Equals(Sign.greaterThan))
                {
                    for (int i = 0; i < additionalVarQuantity; i++)
                        if (i.Equals(basicVarPos))
                            restrictionVar[j, varQuantity + i] = -1f;
                        else restrictionVar[j, varQuantity + i] = 0f;
                    basicVarPos++;
                    for (int i = 0; i < varQuantity + additionalVarQuantity; i++)
                        restrictionVar[j, i] *= -1f;
                    freeVar[j] *= -1f;
                }
                else if (signs[j].Equals(Sign.lessThan))
                {
                    for (int i = 0; i < additionalVarQuantity; i++)
                        if (i.Equals(basicVarPos))
                            restrictionVar[j, varQuantity + i] = 1f;
                        else restrictionVar[j, varQuantity + i] = 0f;
                    basicVarPos++;

                }
            }
            Array.Resize<double>(ref mainFuncVar, freeVar.Length + additionalVarQuantity);
            for (int i = varQuantity; i < varQuantity + additionalVarQuantity; i++)
                mainFuncVar[i] = 0;


        }

        private bool searchForBasic()
        {
            //Searching for basic
            basicCollums = new bool[varQuantity + additionalVarQuantity];
            basicIndices = new int[restrictionQuantity];
            for (int i = 0; i < varQuantity + additionalVarQuantity; i++)
            {
                basicCollums[i] = false;
                for (int j = 0; j < restrictionQuantity; j++)
                {
                    if (restrictionVar[j, i].Equals(1) && !basicCollums[i])
                    {
                        basicIndices[j] = i;
                        basicCollums[i] = true;
                    }
                    else if (restrictionVar[j, i].Equals(1) && basicCollums[i])
                    {
                        basicCollums[i] = false;
                        break;
                    }
                    else if (!restrictionVar[j, i].Equals(0) && !restrictionVar[j, i].Equals(1))
                    {
                        basicCollums[i] = false;
                        break;
                    }
                }
            }

            
            foreach (bool temp in basicCollums)
            {
                if (temp) basicVarQuantity++;
            }
            nonBasicVarQuantity = (varQuantity + additionalVarQuantity) - basicVarQuantity;

            //Filling non basic array
            nonBasicIndices = new int[nonBasicVarQuantity];
            for (int i = 0, h = 0; i < varQuantity + additionalVarQuantity; i++ )
            {
                if (!basicIndices.Contains<int>(i))
                {
                    nonBasicIndices[h] = i;
                    h++;
                }    
            }
            //Checking if basic is found
            if (basicVarQuantity.Equals(restrictionQuantity))
                return true;
            else return false;


        }

        private void simplexRate()
        {
            funcVarRate = new double[varQuantity + additionalVarQuantity];
            double sum = 0;
            for (int i = 0; i < nonBasicVarQuantity; i++)
            {
                sum = 0;
                for (int j = 0; j < basicVarQuantity; j++)
                    sum += mainFuncVar[basicIndices[j]] * restrictionVar[j, i];
                funcVarRate[i] = sum - mainFuncVar[nonBasicIndices[i]];
            }
            sum = 0;
            for (int j = 0; j < basicVarQuantity; j++)
                sum += mainFuncVar[basicIndices[j]] * freeVar[j];
            funcFreeVarRate = sum;
        }
        
        private void printTable()
        {
            string streamString = "";
            for (int i = 0; i < nonBasicVarQuantity; i++)
                streamString += "\t\tX" + (nonBasicIndices[i] + 1);
            for (int j = 0; j < restrictionQuantity; j++)
            {
                streamString += "\n";
                streamString += "X" + (basicIndices[j] + 1) + "\t\t";
                for (int i = 0; i < nonBasicVarQuantity; i++)
                {
                    streamString += restrictionVar[j, i].ToString("0.0") + "\t\t";
                }
                streamString += freeVar[j].ToString("0.0") + "\t\t";
            }
            streamString += "\n";
            streamString += "F(x)\t\t";
            for ( int i = 0; i < nonBasicVarQuantity; i++)
                streamString += funcVarRate[i].ToString("0.0") + "\t\t";
            streamString += funcFreeVarRate.ToString("0.0") + "\n";

            resultString += "\n" + streamString;
        }

        private int minSimplexRatio(int mainElementI)
        {
            double simplexRatio = double.MaxValue;
            int mainElementJ = 0;
            for (int j = 0; j < restrictionQuantity; j++)
            {
                if (freeVar[j] > 0 && restrictionVar[j, mainElementI] > 0)
                {
                    if (freeVar[j] /restrictionVar[j, mainElementI] < simplexRatio)
                    {
                        simplexRatio = freeVar[j] / restrictionVar[j, mainElementI];
                        mainElementJ = j;
                    }
                    else continue;
                }     
                else continue;
            }
            return mainElementJ;
        }
        
        private void JordanConversion(int elementJ, int elementI)
        {
            mainElement = restrictionVar[elementJ, elementI];
            for ( int j = 0; j < basicVarQuantity; j++)
                for ( int i = 0; i < nonBasicVarQuantity; i++)
                    if (i != elementI && j != elementJ)
                        restrictionVar[j, i] = ((mainElement * restrictionVar[j, i]) - (restrictionVar[j, elementI] * restrictionVar[elementJ, i])) / mainElement;
            for ( int j = 0; j < basicVarQuantity; j++)
                if (j != elementJ)
                    freeVar[j] = ((mainElement * freeVar[j]) - (restrictionVar[j, elementI] * freeVar[elementJ])) / mainElement;
            for ( int i = 0; i < basicVarQuantity; i++)
                if (i != elementI)
                    funcVarRate[i] = ((mainElement * funcVarRate[i]) - (restrictionVar[elementJ, i] * funcVarRate[elementI])) / mainElement;
            funcFreeVarRate = ((mainElement * funcFreeVarRate) - (freeVar[elementJ] * funcVarRate[elementI])) / mainElement;
            for ( int j = 0; j < basicVarQuantity; j++)
                restrictionVar[j, elementI] /= -mainElement;
            for ( int i = 0; i < nonBasicVarQuantity; i++)
                restrictionVar[elementJ, i] /= mainElement;
            restrictionVar[elementJ, elementI] = 1 / mainElement;
            funcVarRate[elementI] /= -mainElement;
            freeVar[elementJ] /= mainElement;
        }

        private void printResult()
        {
            string streamString = "";
            streamString += "X* = (";
            for (int i = 0; i < basicVarQuantity + nonBasicVarQuantity; i++)
                if (nonBasicIndices.Contains(i))
                    if (i.Equals(basicVarQuantity + nonBasicVarQuantity - 1))
                        streamString += "0.0)";
                    else streamString += "0.0, ";
                else if (basicIndices.Contains(i))
                    if (i.Equals(basicVarQuantity + nonBasicVarQuantity - 1))
                        streamString += freeVar[Array.IndexOf(basicIndices, i)].ToString("0.0") + ")";
                    else streamString += freeVar[Array.IndexOf(basicIndices, i)].ToString("0.0") + ", ";
            streamString += "\nF(x) = " + funcFreeVarRate.ToString("0.0");
            resultString += "\n" + streamString + "\n";
        }

        private void arr2DResize<T>(ref T[,] original, int newJ, int newI)
        {
            var newArr = new T[newJ, newI];
            int minRows = Math.Min(newJ, original.GetLength(0));
            int minCols = Math.Min(newI, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
                for (int j = 0; j < minCols; j++)
                    newArr[i, j] = original[i, j];
            original = newArr;
        }
        
    }
   
    public enum Sign
    {
        lessThan,
        equally,
        greaterThan
    }

    public enum Aim
    {
        MIN,
        MAX
    }

}
